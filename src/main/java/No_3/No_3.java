package No_3;

class Mahasiswa{
    String nama, nim, alamat, hobi;
}
public class No_3 {
    public static void main(String[] args) {
        Mahasiswa Ade = new Mahasiswa();
        Mahasiswa Pras = new Mahasiswa();

        Ade.nama = "Ade Safarudin Madani";
        Ade.nim = "F1B019005";
        Ade.alamat = "Jln. Wisma Seruni V No.4";
        Ade.hobi = "Tidur";


        Pras.nama = "Prasetyo Asnawy Pangestu";
        Pras.nim = "F1B019114";
        Pras.alamat = "Jl. Karang Panas";
        Pras.hobi = "Tidur";

        System.out.println("Nama : "+Ade.nama);
        System.out.println("NIM : "+Ade.nim);
        System.out.println("Alamat : "+Ade.alamat);
        System.out.println("Hobi : "+Ade.hobi);
        System.out.println(" ");
        System.out.println("Nama : "+Pras.nama);
        System.out.println("NIM : "+Pras.nim);
        System.out.println("Alamat : "+Pras.alamat);
        System.out.println("Hobi : "+Pras.hobi);
    }
}
