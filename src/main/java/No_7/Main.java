package No_7;

class Sepatu {
    private static String namaSepatu;
    private static int nilaiHarga;

    public Sepatu(String nama, int harga){
        namaSepatu = nama;
        nilaiHarga = harga;
    }

    public String getNamaSepatu() {
        return namaSepatu;
    }
    public int getNilaiHarga(){
        return nilaiHarga;
    }
}

public class Main{
    public static void main(String[] args) {
        Sepatu sepatu1 = new Sepatu("Adidas",5000);
        Sepatu sepatu2 = new Sepatu("Nike", 7000);
        Sepatu sepatu3 = new Sepatu("Polo", 8000);
        Sepatu sepatu4 = new Sepatu("New Era", 7600);
        Sepatu sepatu5 = new Sepatu("Pro ATT", 5800);

        System.out.println("Daftar Belanja Sepatu");
        System.out.println("Nama barang | Harga barang");
        System.out.print(sepatu1.getNamaSepatu()+"\t\t| ");
        System.out.println(sepatu1.getNilaiHarga());
        System.out.print(sepatu2.getNamaSepatu()+"\t\t| ");
        System.out.println(sepatu2.getNilaiHarga());
        System.out.print(sepatu3.getNamaSepatu()+"\t\t| ");
        System.out.println(sepatu3.getNilaiHarga());
        System.out.print(sepatu4.getNamaSepatu()+"\t\t| ");
        System.out.println(sepatu4.getNilaiHarga());
        System.out.print(sepatu5.getNamaSepatu()+"\t\t| ");
        System.out.println(sepatu5.getNilaiHarga());
    }
}
